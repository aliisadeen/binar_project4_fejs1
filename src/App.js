import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Navbar from './components/Navbar';
import Detail from './components/Detail';
import Landing from './pages/Landing';
import Other from './pages/Other';
import Search from './components/Search';

function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <Routes>
        <Route path='/' element={<Landing/>}/>
        <Route path='/movies' element={<Other/>}/>
        <Route path='/detail' element={<Detail />} />
        <Route path='/search' element={<Search />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
